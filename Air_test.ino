#include <Wire.h>
#include <SFE_BMP180.h>    //new
#include <EEPROM.h>


#define onState                     1
#define chanel_number               9
#define default_servo_value      1500
#define PPM_FrLen               22500
#define PPM_PulseLen              300
#define SIGNAL_PIN                  8
#define PIN_LED                    17
#define PIN_LED_Y                   7
#define PIN_BUZ                    A1
#define PIN_IN                      2
#define PIN_CAM                     3
/*=========================================================================================================================================*/
SFE_BMP180 pressure;      //new
double P0 = 0, P, Alt;            //new
int cycle = 0;
int w = 1;
int unsigned PWMin = 0;
byte i;
byte a = 0;
byte AltP = 0;
boolean Flay = false;
boolean Shut = true;
int ppm[chanel_number] = {1524, 1495, 1036, 1496, 996, 996, 996, 1516, 1496};

/*=========================================================================================================================================*/
void shutOpen() {
  OCR0B = 24;
}
void shutClose() {
  OCR0B = 13;
}
void buzPik() {
  analogWrite(PIN_BUZ, 150);
  digitalWrite(PIN_LED, HIGH);
  delay(15);
  analogWrite(PIN_BUZ, 0);
  digitalWrite(PIN_LED, LOW);
  delay(3);
}void ledYelow() {
  digitalWrite(PIN_LED_Y, HIGH);
  //analogWrite(PIN_BUZ, 150);
  delay(30);
  //analogWrite(PIN_BUZ, 0);
  digitalWrite(PIN_LED_Y, LOW);
  delay(30);
}
void setup() {
  pressure.begin();    // new
  P0 = getPressure();  //new
  Serial.begin(115200);
  pinMode (PIN_LED, OUTPUT);
  pinMode (PIN_LED_Y, OUTPUT);
  pinMode (5, OUTPUT);
  pinMode (PIN_BUZ, OUTPUT);
  pinMode(PIN_IN, INPUT);
  pinMode(PIN_CAM, OUTPUT);
  pinMode(SIGNAL_PIN, OUTPUT);
  digitalWrite(PIN_LED, LOW);
  digitalWrite(PIN_LED_Y, LOW);
  digitalWrite(PIN_CAM, LOW);
  analogWrite(PIN_BUZ, 0);
  digitalWrite(SIGNAL_PIN, !onState);
  
  cli();
  TCCR2A = 0;
  TCCR2B = 0;

  EIMSK  =  (1 << INT0); // разрешение прерываний INT0 и INT1
  EICRA  =  (1 << ISC00); // настройка срабатывания прерываний на любому изменению

  TCCR1A = 0;
  TCCR1B = 0;
  OCR1A = 100;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS11);
  TIMSK1 |= (1 << OCIE1A);
  
  
  TCCR0A = (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
  shutClose();
  TCCR0B = (1 << CS02) | (1 << CS00);
  sei();

  delay(100);
  if ( 850 < PWMin && PWMin < 950 )
    i = 1;
  while ( 850 < PWMin && PWMin < 950 ) {
    ledYelow();
    delay(50);
    i++;
    EEPROM.write(1, i);
  }
  a = EEPROM.read(1);
  AltP = a * 10 + 10;
  for (w = 1; w < a; w++) {
    ledYelow();
    Serial.print(w);
    Serial.print("\n\r");
  }
}
/*==============================================================================================================================================*/
void loop()
{
  P = getPressure();
  Alt = pressure.altitude(P, P0);
    Serial.print(Alt);
    Serial.print("\n\r");
  if (1450 < PWMin && PWMin < 1550)
  {
    buzPik(); buzPik();

    Serial.println(Alt, 2);

    if (Alt > AltP) {                   // take safe altitude
      Flay = true;
      digitalWrite(PIN_CAM, HIGH);
      Serial.print("CamON");              /////////
      Serial.print("\n\r");               /////////
    }
  }  //-------------------------------------------------------------------

  if (Flay) {
    if (Alt < AltP) {
      digitalWrite(PIN_CAM, LOW);
      Serial.print("CamOFF");    ///////
      Serial.print("\n\r");      ///////
      if (Alt < AltP - 10) {
        Serial.print("EnginOFF");
        Serial.print("\n\r");
        delay(40);
        shutOpen();
        while (1) {}
      }
    }
  }

  if (1850 < PWMin && PWMin < 1950) {
    digitalWrite(PIN_CAM, LOW);
    Serial.print("CamOFF");       //////
    Serial.print("\n\r");         //////
    Serial.print("EnginOFF");
    Serial.print("\n\r");
    delay(500);
    shutOpen();
    while (1) {}
  }
  delay(50);
}
/*=========================================================================================================*/
ISR(INT0_vect)
{
  boolean state = digitalRead(2);
  if (state) {
    TCNT2 = 0;
    TCCR2B |= (1 << CS21) | (1 << CS22);
    PWMin = 0;
  }
  else  {
    TCCR2B = 0;
    PWMin = TCNT2 * 16;
    Serial.print(PWMin);
    Serial.print("\n\r");
  }
}
/*==============================================================================================================*/
double getPressure() {                 //new
  char status;
  double T, P, p0, a;

  status = pressure.startTemperature();
  if (status != 0) {
    delay(status);                           // ожидание замера температуры
    status = pressure.getTemperature(T);
    if (status != 0) {
      status = pressure.startPressure(3);
      if (status != 0) {
        delay(status);                    // ожидание замера давления
        status = pressure.getPressure(P, T);
        if (status != 0) {
          return (P);
        }
      }
    }
  }
}
ISR(TIMER1_COMPA_vect)
{
  static boolean state = true;
  TCNT1 = 0;
  if (state)
  {
    digitalWrite(SIGNAL_PIN, onState);
    OCR1A = PPM_PulseLen * 2;
    state = false;
  }
  else
  {
    static byte cur_chan_numb;
    static unsigned int calc_rest;

    digitalWrite(SIGNAL_PIN, !onState);
    state = true;

    if (cur_chan_numb >= chanel_number) {
      cur_chan_numb = 0;
      calc_rest = calc_rest + PPM_PulseLen;//
      OCR1A = (PPM_FrLen - calc_rest) * 2;
      calc_rest = 0;
    }
    else {
      OCR1A = (ppm[cur_chan_numb] - PPM_PulseLen) * 2;
      calc_rest = calc_rest + ppm[cur_chan_numb];
      cur_chan_numb++;
    }
  }
}
